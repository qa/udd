#!/usr/bin/ruby

FETCH_LIMIT = 5000

$:.unshift('../rlibs')
require 'udd-web'

def esc(s)
  return "" if s.nil?
  CGI.escape_html(s)
end


#puts "Content-type: text/plain\n\n"; STDERR.reopen(STDOUT)

puts "Content-type: text/html; charset=utf-8\n\n"

now = Time::now
cgi = CGI::new

if not cgi['tag'].empty?
  tag = esc(cgi['tag'])
else
  tag = nil
end

affected = (cgi['affected'] == 'yes')

if tag.nil?
  # list all tags
  q = <<-EOF
select distinct tag_type, tag, coalesce(affected_sources, 0) as affected
FROM lintian_tags_descriptions
left join
(select tag, count(distinct source) as affected_sources
from lintian_results_agg
group by tag) tag_affected using (tag)
ORDER BY tag ASC
  EOF
  DB = Sequel.connect(UDD_GUEST)
  res = DB[q].map { |r| r.to_h }

  pagename = 'lintian-tag' ; title = 'Lintian tags information' ; canonical = '/lintian-tags/'
  description = 'Debian Lintian Tag List'
  puts ERB.new(File.read(File.expand_path('templates/partials/header.erb'))).result(binding)
  puts <<-EOF
<div id="content">
<h1>Debian Lintian tags list</h1>

<table class="display compact cell-border" id="lintian-tags-list">
<thead>
<tr>
    <th>tag</th>
    <th>level</th>
    <th>affected source packages</th>
</tr>
</thead>
<tbody>
  EOF
  res.each do |r|
    puts "<tr><td><a href=\"/lintian-tag/#{CGI.escape(r[:tag])}\">#{esc(r[:tag])}</a></td>"
    puts "<td>#{esc(r[:tag_type])}</td>"
    puts "<td>#{r[:affected]}</td></tr>"
  end
  puts <<-EOF
</tbody>
</table>
</div>
<br>
<br>
<br>
  EOF
  puts ERB.new(File.read(File.expand_path('templates/partials/footer.erb'))).result(binding)
  exit(0)
end

if tag !~ /^[a-z0-9_.+\/-]+$/i
  puts "Invalid characters in tag name"
  exit(0)
end

DB = Sequel.connect(UDD_GUEST)

if affected
  q = <<-EOF
select source, version, package, package_version, architectures::text[] architectures, tag_type, tag, information, lintian_version, count
FROM lintian_results_agg
WHERE tag = '#{tag}'
ORDER BY 1,2,3,4,6,7
LIMIT #{FETCH_LIMIT}
  EOF
  res = DB[q].map { |r| r.to_h }
  res.each do |e|
    # convert array to string (that's something that is only needed in the dev env, strangely)
    if defined?(Sequel::Postgres::PGArray) and e[:architectures].kind_of?(Sequel::Postgres::PGArray)
      e[:architectures] = e[:architectures].join(',')
    end
    e[:architectures] = e[:architectures].gsub('NULL', 'source').gsub(/^\{(.*)\}$/, '\1').split(',').uniq.sort
  end
end

q = "select tag_type, description from lintian_tags_descriptions where tag='#{tag}'"
res2 = DB[q].first
if res2.nil?
  desc = "TAG NOT KNOWN"
  type = "UNKNOWN"
else
  desc = res2[:description]
  type = res2[:tag_type]
end

pagename = 'lintian-tag'
title = "Lintian tag: #{tag}"
canonical = "/lintian-tag/#{tag}#{affected ? '?affected=yes' : ''}"
description = "Debian Lintian Tag information for tag #{tag}"
puts ERB.new(File.read(File.expand_path('templates/partials/header.erb'))).result(binding)

puts <<-EOF
<div id="content">
<h1>Lintian tag: #{tag}</h1>
<h2>Type: #{esc(type)}</h2>
<h2>Description (from <code>lintian-explain-tags</code>)</h2>
<pre>#{esc(desc)}</pre>
EOF
if not affected
  puts "<p><a href=\"/lintian-tag/#{tag}?affected=yes\"><b>Show affected packages</b></a></p>"
else
  puts <<-EOF
<h2>Affected packages</h2>
<table class="display compact cell-border" id="lintian-tag-affected-packages">
<thead>
<tr>
    <th>source</th>
    <th>version</th>
    <th>binary</th>
    <th>level</th>
    <th>tag</th>
    <th><span title="tags affecting binary packages are counted once per architecture">count</th>
    <th>information</th>
</tr>
</thead>
<tbody>
  EOF
  res.each do |t|
    if t[:package]
      bin = "<span title=\"#{t[:architectures].join(',')}\">#{esc(t[:package])}/#{esc(t[:package_version])}</span>"
    else
      bin = ""
    end
    puts <<-EOF
    <tr>
      <td class="nowrap"><a href="https://tracker.debian.org/#{esc(t[:source])}">#{esc(t[:source])}</a></td>
      <td class="nowrap">#{esc(t[:version])}</td>
      <td class="nowrap">#{bin}</td>
      <td class="nowrap">#{esc(t[:tag_type])}</td>
      <td class="nowrap">#{esc(t[:tag])}</td>
      <td class="nowrap">#{t[:count]}</td>
      <td class="nowrap">#{esc(t[:information])}</td>
    </tr>
    EOF
  end
  puts <<-EOF
</tbody>
</table>
  EOF
  if res.length == FETCH_LIMIT
    puts "<p>Output limited to the first #{FETCH_LIMIT} results.</p>"
  end
end
puts <<-EOF
</div>
<br>
<br>
<br>
EOF
puts ERB.new(File.read(File.expand_path('templates/partials/footer.erb'))).result(binding)

