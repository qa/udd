#!/usr/bin/ruby

$:.unshift('../../rlibs')
require 'udd-web'

#puts "Content-type: text/plain\n\n"; STDERR.reopen(STDOUT)

DB = Sequel.connect(UDD_GUEST)
cgi = CGI.new

format = (cgi['format'] == 'json' ? :json : :html)
user = cgi['user']
user = nil if user.empty?
user = CGI.escape_html(user) if not user.nil?

tag = cgi['tag']
tag = nil if tag.empty?
tag = CGI.escape_html(tag) if not tag.nil?

bug = cgi['bug']
bug = nil if bug.empty?
bug = CGI.escape_html(bug) if not bug.nil?

browse_users = (cgi['browse'] == 'users')

def html_header
  puts "Content-type: text/html\n\n"
  pagename = 'bugs-usertags' ; title = 'Bugs Usertags'
  puts ERB.new(File.read(File.expand_path('../templates/partials/header.erb'))).result(binding)

  puts <<-EOF
<div id="content">
    <h1>Bugs Usertags Browser</h1>
  EOF
end

def html_footer
  puts <<-EOF
<h2>Search by user and tag</h2>
<form action="?" accept-charset="UTF-8">
<table>
<tr><td>
  <label for="user">User:</label>
</td><td>
  <input type="email" name="user" id="user">
</td></tr>
<tr><td>
  <label for="tag">Tag (optional):</label>
</td><td>
  <input type="text" name="tag" id="tag"><br />
</td></tr>
<tr><td colspan=2>
  <input type="submit" formnovalidate value="Search">
</td></tr>
</table>
</form>
<h2>Search by bug</h2>
<form action="?" accept-charset="UTF-8">
  <label for="bug">Bug:</label>
  <input type="number" name="bug" id="bug" placeholder="Bug #">
  <input type="submit" value="Search">
</form>
<h2>Browse</h2>
<ul>
  <li><a href="?browse=users">By user</a></li>
</ul>
</div>
  EOF
  pagename = 'bugs-usertags'
  puts ERB.new(File.read(File.expand_path('../templates/partials/footer.erb'))).result(binding)
end

if browse_users
  rows = DB["SELECT email, COUNT(distinct id) AS count, count(distinct tag) as tagcount FROM bugs_usertags INNER JOIN bugs USING (id) GROUP BY email ORDER BY email"].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h2>Users list</h2>"
    puts '<table class="display compact cell-border nowrap" id="btstags-users">'
    puts '<thead><tr><th>User</th><th>Tags used</th><th>Bugs tagged</th></tr></thead>'
    puts '<tbody>'
    rows.each do |r|
      puts "<tr><td><a href=\"?user=#{CGI.escape_html(r['email'])}\">#{CGI.escape_html(r['email'])}</a></td><td>#{r['tagcount']}</td><td>#{r['count']}</td></tr>"
    end
    puts "</tbody></table>"
    puts html_footer
  end

elsif user and not tag
  rows = DB["SELECT tag, COUNT(*) AS count FROM bugs_usertags INNER JOIN bugs USING (id) WHERE email = ? GROUP BY tag ORDER BY tag", user].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h2>User #{CGI.escape_html(user)}</h2>"
    puts '<table class="display compact cell-border nowrap" id="btstags-user">'
    puts '<thead><tr><th>Tag</th><th>Bugs</th></tr></thead><tbody>'
    rows.each do |r|
      puts "<tr><td><a href=\"?user=#{CGI.escape_html(user)}&tag=#{CGI.escape_html(r['tag'])}\">#{CGI.escape_html(r['tag'])}</a></td><td>#{r['count']}</td></tr>"
    end
    puts "</tbody></table>"
    puts html_footer
  end

elsif user and tag
  rows = DB["SELECT id, package, source, title, done != '' AS done, severity FROM bugs INNER JOIN bugs_usertags USING (id) WHERE bugs_usertags.email = ? AND bugs_usertags.tag = ? ORDER BY id", user, tag].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h2>Tagged <i>#{CGI.escape_html(tag)}</i> by <i>#{CGI.escape_html(user)}</i></h2>"
    puts '<table class="display compact cell-border" id="btstags-usertag">'
    puts '<thead><tr><th>State</th><th>Bug</th><th>Severity</th><th>Source</th><th>Package</th><th>Title</th><th class="nowrap">Other tags</th></tr></thead>'
    puts '<tbody>'
    rows.each do |r|
      state = r['done'] ? 'done' : 'open'
      puts "<tr><td>#{state}</td>"
      puts "<td data-sort=\"#{r['id']}\" class=\"nowrap\"><a href=\"https://bugs.debian.org/#{r['id']}\">##{r['id']}</td>"
      puts "<td data-sort=\"#{severity2sort(r['severity'])}\" class=\"nowrap\">#{r['severity']}</td>"
      puts "<td class=\"nowrap\"><a href=\"https://tracker.debian.org/#{CGI.escape_html(r['source'])}\">#{CGI.escape_html(r['source'])}</td>"
      puts "<td class=\"nowrap\"><a href=\"https://tracker.debian.org/#{CGI.escape_html(r['source'])}\">#{CGI.escape_html(r['package'])}</td>"
      puts "<td>#{CGI.escape_html(r['title'])}</td>"
      puts "<td class=\"nowrap\"><a href=\"?bug=#{r['id']}\">list usertags</a></td></tr>"
    end
    puts "</tbody></table>"
    puts html_footer
  end

elsif bug
  rows = DB["SELECT email, tag FROM bugs_usertags WHERE id = ? ORDER BY email, tag", bug].all.sym2str
  if format == :json
    puts "Content-type: application/json\n\n"
    puts JSON.pretty_generate(rows)
  else
    puts html_header
    puts "<h2>Bug ##{CGI.escape_html(bug)}</h2>"
    puts '<table class="display compact cell-border nowrap" id="btstags-bug">'
    puts '<thead><tr><th>User</th><th>Tag</th></tr></thead><tbody>'
    rows.each do |r|
      puts "<tr>"
      puts "<td><a href=\"?user=#{CGI.escape_html(r['email'])}\">#{CGI.escape_html(r['email'])}</a></td>"
      puts "<td><a href=\"?user=#{CGI.escape_html(r['email'])}&tag=#{CGI.escape_html(r['tag'])}\">#{CGI.escape_html(r['tag'])}</a></td>"
      puts "</tr>"
    end
    puts "</tbody></table>"
    puts html_footer
  end
else
  puts html_header
  puts html_footer
end
