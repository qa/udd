#!/usr/bin/ruby
# encoding: utf-8

Encoding.default_external = Encoding::UTF_8

$:.unshift('../rlibs')
$:.unshift('/srv/udd.debian.org/udd/rlibs')
require 'udd-db'
require 'pp'
require 'cgi'
require 'time'
require 'yaml'
require 'json'

STDERR.reopen(STDOUT)
puts "Content-type: text/plain; charset=utf-8\n\n"

tstart = Time::now
DB = Sequel.connect(UDD_GUEST)
DB["SET client_encoding TO 'SQL-ASCII'"]
DB["SET statement_timeout TO 90000"]
DB.extension :pg_array

def dbget(q, *args)
  return DB.fetch(q, *args).all.sym2str
end

rows = dbget <<-EOF
SELECT id, source, severity, title, affected_sources, affected_packages, status, affects_unstable, affects_testing, affects_stable, affects_experimental, last_modified
from bugs
where severity >= 'serious'
or title ~ '(ftbfs|buil(d|t))'
or id in (select id from bugs_tags where tag='ftbfs')
order by id asc
EOF

rows.each do |r|
  r['title'] = r['title'].encode(Encoding::UTF_8, invalid:  :replace, undef: :replace)
  r['affected_sources'] = r['affected_sources'].split(',')
  r['affected_packages'] = r['affected_packages'].split(',')
end

puts JSON.pretty_generate(rows)
