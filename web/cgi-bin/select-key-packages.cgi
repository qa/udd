#!/usr/bin/ruby

$:.unshift('../../rlibs')
require 'udd-db'
require 'cgi'

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)
kp = DB["select source from key_packages"].all.hash_values.flatten

ap = $stdin.readlines.map { |l| l.chomp }
puts (kp & ap).sort
