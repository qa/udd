#!/usr/bin/ruby
# Used by DDPO

$:.unshift('../../rlibs')
require 'udd-db'

puts "Content-type: text/plain\n\n"

DB = Sequel.connect(UDD_GUEST)
DB["SELECT coalesce(tbugs.package, tpatches.package) as package, coalesce(bugs, 0) as bugs, coalesce(patches, 0) as patches
from (select package, count(distinct bugs.bug) as bugs
from ubuntu_bugs_tasks tasks,ubuntu_bugs bugs
where tasks.bug = bugs.bug
and distro in ('Ubuntu')
and status not in ('Invalid', 'Fix Released', 'Won''t Fix', 'Opinion')
group by package) tbugs
full join
(select package, count(distinct bugs.bug) as patches
from ubuntu_bugs_tasks tasks,ubuntu_bugs bugs
where tasks.bug = bugs.bug
and distro in ('', 'Ubuntu')
and status not in ('Invalid', 'Fix Released', 'Won''t Fix', 'Opinion')
and bugs.patches is true
group by package) tpatches on tbugs.package = tpatches.package order by package asc"].all.sym2str.each do |row|
  puts "#{row['package']}|#{row['bugs']}|#{row['patches'] || 0}"
end
