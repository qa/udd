#!/usr/bin/ruby
$:.unshift('../rlibs')
require 'udd-web'
require 'page'
require 'debian'
require 'digest/md5'

# testing migration checks
MIN_AGE_IN_DEBIAN = 11
MIN_SYNC_INTERVAL = 11

PIUPARTS_IGNORED_SECTIONS=%w{
 jessie
 jessie2bpo
 jessie2bpo2stretch
 jessie2lts
 jessie2stretch
 jessie2stretch-rcmd
 jessie2Xstretch
 jessie2Xstretch-rcmd
 jessie-lts2stretch
 jessie-rcmd
 jessie-security
 lenny2squeeze
 squeeze
 squeeze2bpo2wheezy
 squeeze2bpo-sloppy
 squeeze2squeeze-lts
 squeeze2wheezy
 squeeze2wheezy-lts
 stretch
 stretch2bpo
 stretch2bpo2buster
 stretch2buster
 stretch2buster-rcmd
 stretch2next
 stretch2Xbuster
 stretch2Xbuster-rcmd
 stretch-pu
 stretch-rcmd
 stretch-security
 wheezy
 wheezy2bpo2jessie
 wheezy2jessie
 wheezy2jessie-lts
 wheezy2jessie-rcmd
 wheezy2lts
 wheezy-security
}.map { |e| "'#{e}'" }.join(', ')

class DateTime
  def to_date
    return Date::new(year, month, day)
  end
end

class UDDData
  attr_accessor :debug
  attr_reader :sources, :versions, :all_bugs, :bugs_tags, :bugs_count, :migration, :buildd, :dmd_todos, :ubuntu_bugs, :autoremovals, :qa, :ubuntu, :ustb, :udev, :dr, :doostb, :dostb, :dstb, :dtst, :lintian, :security_issues

  def UDDData.parse_cgi_params(cgiparams)
    three = {'1' => '', '2' => '', '3' => ''}
    params = {'email'       => three.clone,
              'nosponsor'   => three.clone,
              'nouploader'  => three.clone,
              'emails'      => {},
              'packages'    => '',
              'bin2src'     => '',
              'ignpackages' => '',
              'ignbin2src'  => '',
              'onlydev' => false,
              'onlyrecent' => false,
              'onlytesting' => false,
              'onlysidnottesting' => false,
              'onlydiffversion' => false,
              'onlykey' => false,
              'lt_error'    => true,
              'lt_warning'    => true,
              'lintian_tag'   => '',
              'querystring' => ''}
    # Handle special case here: only one item specified
    params['querystring'] = URI.encode_www_form(cgiparams.to_a)
    if cgiparams.keys.length == 1 and cgiparams.values.first == [] and not params.keys.include?(cgiparams.keys.first)
      k = cgiparams.keys.first
      if k =~ /@/
        params['email']['1'] = CGI.escapeHTML(k)
        params['emails'][CGI.escapeHTML(k)] = [:maintainer, :uploader, :sponsor]
      else
        params['packages'] = CGI.escapeHTML(k)
      end
      return params
    end
    ['email', 'nosponsor', 'nouploader'].each do |s|
      params[s].each do |k, v|
        p = cgiparams[s + k][0]
        params[s][k] = CGI.escapeHTML(p) if p and p != ''
      end
    end
    params['email'].each do |k, v|
      next if v == ''
      roles = [:maintainer, :uploader, :sponsor]
      roles.delete(:uploader) if params['nouploader'][k] == 'on'
      roles.delete(:sponsor) if params['nosponsor'][k] == 'on'
      params['emails'][v] = roles
    end
    ['packages', 'bin2src', 'ignpackages', 'ignbin2src', 'lintian_tag'].each do |s|
      p = cgiparams[s][0]
      params[s] = CGI.escapeHTML(p) if p
    end
    ['onlyrecent', 'onlydev', 'onlytesting', 'onlysidnottesting', 'onlydiffversion', 'onlykey'].each do |s|
      p = cgiparams[s][0]
      params[s] = (p and p != '')
    end
    # lintian
    one_type_selected = false
    ['error', 'warning', 'information', 'pedantic', 'experimental', 'overridden', 'masked', 'classification'].map { |e| "lt_#{e}" }.each do |s|
      p = cgiparams[s][0]
      params[s] = (p and p != '')
      one_type_selected = true if params[s]
    end
    if not one_type_selected
      params['lt_error'] = true
      params['lt_warning'] = true
    end
    params
  end

  def initialize(emails={}, addsources="", bin2src=false, ignsources="", ignbin2src=false, onlydev = false, onlyrecent = false, onlytesting = false, onlysidnottesting = false, onlydiffversion = false, onlykey = false, debug = false)
    @debug = debug
    @emails = emails
    @addsources = addsources
    @bin2src = bin2src
    @ignsources = ignsources
    @ignbin2src = ignbin2src
    @onlydev = onlydev
    @onlyrecent = onlyrecent
    @onlyrecent_days = 30
    @onlytesting = onlytesting
    @onlysidnottesting = onlysidnottesting
    @onlydiffversion = onlydiffversion
    @onlykey = onlykey


    @dbh = Sequel.connect(UDD_GUEST)
    if @debug
      puts "Content-type: text/html\n\n"
      @totqtime = 0.0
    end

    ur = YAML::load(IO::read('ubuntu-releases.yaml'))
    @dr = {}
    releases_rows = dbget("select * from releases where role != ''")
    releases_rows.each { |r| dr[r['role']] = r['release'] }
    @doostb = dr['oldoldstable'] || ''
    @dostb = dr['oldstable']
    @dstb = dr['stable']
    @dtst = dr['testing']

    @ustb = ur['stable']
    @udev = ur['devel']

    get_sources
  end

  def get_all_data
    get_sources_versions
    get_sources_bugs
    get_migration
    get_buildd
    get_qa
    get_sources_dirty
    get_autoremovals
    get_security_issues
    get_dmd_todos
    get_ubuntu_bugs
    get_ubuntu_dirty
  end

  def get_sources
    @sources = {}
    dbget("create temporary table mysources(source text primary key)")

    if @onlydev
      releases = "'sid'"
    else
      releases = "'#{@dostb}', '#{@dstb}', '#{@dtst}', 'sid'"
    end

    dbget <<-EOF
CREATE TEMPORARY VIEW sources_most_recent AS
select distinct source, version, maintainer_email from sources s1
where release in (#{releases})
and not exists (select * from sources s2
where s1.source = s2.source
and release in (#{releases})
and s2.version > s1.version);
    EOF

    maint_emails = @emails.reject { |k, v| not v.include?(:maintainer) }.keys
    if not maint_emails.empty?
      q = <<-EOF
    select distinct source, maintainer_email from sources_most_recent
    where maintainer_email in (#{maint_emails.map { |e| quote(e) }.join(',')})
    union
    select distinct source, maintainer_email from sources_uniq where release='experimental'
    and maintainer_email in (#{maint_emails.map { |e| quote(e) }.join(',')})
      EOF
      maint_rows = dbget(q)
    else
      maint_rows = []
    end

    dbget <<-EOF
CREATE TEMPORARY VIEW uploaders_most_recent AS
select distinct source, version, email from uploaders s1
where release in (#{releases})
and not exists (select * from uploaders s2
where s1.source = s2.source
and release in (#{releases})
and s2.version > s1.version);
    EOF

    upload_emails = @emails.reject { |k, v| not v.include?(:uploader) }.keys
    if not upload_emails.empty?
      q = <<-EOF
    select distinct source, email from uploaders_most_recent where email in (#{upload_emails.map { |e| quote(e) }.join(',')})
    union
    select distinct source, email from uploaders where release='experimental'
    and email in (#{upload_emails.map { |e| quote(e) }.join(',')})
      EOF
      upload_rows = dbget(q)
    else
      upload_rows = []
    end

    sponsor_emails = @emails.reject { |k, v| not v.include?(:sponsor) }.keys
    if not sponsor_emails.empty?
      q = <<-EOF
      select distinct source, key_id from upload_history uh, carnivore_emails ce, carnivore_keys ck
      where (source, version) in (
         select source, version from sources_most_recent
         union
         select source, version from sources_uniq where release='experimental'
      )
      and ce.email in (#{sponsor_emails.map { |e| quote(e) }.join(',')})
      and ce.id = ck.id
      and uh.fingerprint = ck.key
      EOF
      sponsor_rows = dbget(q)
    else
      sponsor_rows = []
    end

    srcs = {}
    sponsor_rows.each { |e| e = e.values; srcs[e[0]] = [:sponsor, e[1]] }
    upload_rows.each { |e| e = e.values; srcs[e[0]] = [:uploader, e[1]] }
    maint_rows.each { |e| e = e.values; srcs[e[0]] = [:maintainer, e[1]] }

    if @bin2src and @addsources != ''
      q = <<-EOF
      select distinct source from packages
         where package in (#{@addsources.split(/\s/).map { |e| quote(e) }.join(',')})
      EOF
      @addsources = dbget(q).map { |e| e[0] }
    else
      @addsources = @addsources.split(/\s/)
    end
    @addsources.each do |p|
      p.chomp!
      srcs[p] = [:manually_listed]
    end

    if @ignbin2src and @ignsources != ''
      q = <<-EOF
      select distinct source from packages
         where package in (#{@ignsources.split(/\s/).map { |e| quote(e) }.join(',')})
      EOF
      @ignsources = dbget(q).map { |e| e[0] }
    else
      @ignsources = @ignsources.split(/\s/)
    end
    @ignsources.each do |p|
      p.chomp!
      srcs.delete(p)
    end

    if @onlyrecent
      r = dbget <<-EOF
      select source from upload_history where
      source in (#{srcs.keys.map { |e| quote(e) }.join(',')})
      and current_timestamp - interval '#{@onlyrecent_days} days' <= date
      EOF
      filtered_srcs = r.map { |e| e['source'] }
      srcs.delete_if { |k, v| not filtered_srcs.include?(k) }
    end

    if @onlytesting
      r = dbget <<-EOF
      select source from sources_uniq
      where source in (#{srcs.keys.map { |e| quote(e) }.join(',')})
      and release = '#{@dtst}'
      EOF
      filtered_srcs = r.map { |e| e['source'] }
      srcs.delete_if { |k, v| not filtered_srcs.include?(k) }
    end

    if @onlydiffversion
      r = dbget <<-EOF
      select source from sources_uniq su1
      inner join sources_uniq su2 using (source)
      where su1.source in (#{srcs.keys.map { |e| quote(e) }.join(',')})
      and su1.release = '#{@dtst}'
      and su2.release = 'sid'
      and su1.version != su2.version
      EOF
      filtered_srcs = r.map { |e| e['source'] }
      srcs.delete_if { |k, v| not filtered_srcs.include?(k) }
    end

    if @onlysidnottesting
      r = dbget <<-EOF
      select source from sources_uniq
      where source in (#{srcs.keys.map { |e| quote(e) }.join(',')})
      and release = 'sid'
      and source not in (
        select source from sources_uniq
        where release = '#{@dtst}'
      )
      EOF
      filtered_srcs = r.map { |e| e['source'] }
      srcs.delete_if { |k, v| not filtered_srcs.include?(k) }
    end

    if @onlykey
      r = dbget <<-EOF
      select source from key_packages
      EOF
      filtered_srcs = r.map { |e| e['source'] }
      srcs.delete_if { |k, v| not filtered_srcs.include?(k) }
    end

    if not srcs.empty?
      dbget("insert into mysources values (#{srcs.keys.map { |e| quote(e) }.join('),(')})")
      dbget("analyze mysources")
    end

    srcs.each do |k, v|
      if v[0] == :manually_listed
        r = "Manually listed"
      elsif v[0] == :maintainer
        r = "Maintained by #{v[1]}"
      elsif v[0] == :uploader
        r = "Co-maintained by #{v[1]}"
      elsif v[0] == :sponsor
        r = "Was uploaded by #{v[1]}"
      end
      srcs[k].push(r)
    end

    @sources = srcs
  end

  def get_sources_versions
    @versions = {}
    @ready_for_upload = {}
    return @versions if @sources.empty?
    # versions in archives
    q = "select source, version, distribution, release, component from sources_uniq where source in (select source from mysources)"
    rows = dbget(q)
    urels = ['', '-security', '-updates', '-backports', '-proposed'].inject([]) { |a, b| a + [ @ustb + b, @udev + b ] }.map { |e| "'#{e}'" }.join(',')
    q = "select source, version, distribution, release, component from ubuntu_sources where source in (select source from mysources) and release in (#{urels})"
    rows += dbget(q)

    rows.each do |r|
      @versions[r['source']] ||= {}
      @versions[r['source']][r['distribution']] ||= {}
      @versions[r['source']][r['distribution']][r['release']] = { :version => r['version'], :component => r['component'] }
    end

    # upstream versions
    q = "select source, distribution, release, upstream_version, status, warnings, errors from upstream where source in (select source from mysources) and status is not null"
    rows = dbget(q)
    rows.group_by { |r| r['source'] }.each_pair do |k, v|
      unst = v.select { |l| l['release'] == 'sid' }.first
      exp = v.select { |l| l['release'] == 'experimental' }.first
      if unst != nil and exp != nil
        us = unst['status']
        es = exp['status']
        if us == 'error' or es == 'error'
          st = :error
        elsif us == 'Debian version newer than remote site' or es == 'Debian version newer than remote site' or
              us == 'only older package available' or es == 'only older package available'
          st = :newer_in_debian
        elsif us == 'up to date'
          st = :up_to_date
        elsif es == 'up to date'
          st = :out_of_date_in_unstable
        else
          st = :out_of_date
        end
        @versions[unst['source']]['upstream'] = { :status => st, :version => unst['upstream_version'], :warnings => CGI.escape_html(unst['warnings'] || ''), :errors => CGI.escape_html(unst['errors'] || '') }
      elsif unst.nil? and exp != nil
        r = exp
        st = case r['status']
             when 'up to date' then :up_to_date
             when 'Debian version newer than remote site', 'only older package available' then :newer_in_debian
             when 'Newer version available', 'newer package available' then :out_of_date
             when 'error' then :error
             else nil
             end
        @versions[r['source']]['upstream'] = { :status => st, :version => r['upstream_version'], :warnings => CGI.escape_html(r['warnings'] || ''), :errors => CGI.escape_html(r['errors'] || '') }
      elsif unst != nil and exp == nil
        r = unst
        st = case r['status']
             when 'up to date' then :up_to_date
             when 'Debian version newer than remote site', 'only older package available' then :newer_in_debian
             when 'Newer version available', 'newer package available' then :out_of_date
             when 'error' then :error
             else nil
             end
        @versions[r['source']]['upstream'] = { :status => st, :version => r['upstream_version'], :warnings => CGI.escape_html(r['warnings'] || ''), :errors => CGI.escape_html(r['errors'] || '') }
      else
        puts "ERROR"
        p v
        exit(1)
      end
    end

    # fresh-releases versions
    q = <<-EOF
WITH freshrel_sources AS NOT MATERIALIZED (
 select distinct source, version from unofficial_sources where distribution='debian-janitor' and release ='fresh-releases' and source in (select source from mysources)
)
select u.source, u.version, upstream_version, fs.version as fs_version
from upstream u
inner join freshrel_sources fs on fs.source = u.source and status='newer package available' and fs.version > u.version
where u.source in (select source from mysources);
    EOF
    rows = dbget(q)
    rows.each do |r|
      @versions[r['source']]['fresh-releases'] = { :version => r['fs_version'] }
    end

    # vcs versions
    q = "select source, changelog_version, changelog_distribution from vcswatch where source in (select source from mysources)"
    rows = dbget(q)
    rows.each do |r|
      @versions[r['source']]['vcs'] = { :version => r['changelog_version'], :distribution => r['changelog_distribution'] }
    end

    q = <<-EOF
select source, changelog_version, browser from vcswatch
where source in (select source from mysources)
and source in (select source from sources_uniq where release in ('sid', 'experimental'))
and changelog_distribution!='UNRELEASED' and changelog_version > coalesce((select max(version) from sources_uniq where release in ('sid','experimental') and sources_uniq.source = vcswatch.source), 0::debversion)
EOF
    dbget(q).each do |r|
      @ready_for_upload[r['source']] = r.to_h
    end

    # mentors.d.n versions
    q = "select package, version, distribution, uploaded from mentors_most_recent_package_versions mpv where mpv.package in (select source from mysources)"
    rows = dbget(q)
    rows.each do |r|
      @versions[r['package']]['mentors'] = { :version => r['version'], :distribution => r['distribution'], :uploaded => r['uploaded'] }
    end
  end

  def get_lintian(params)
    tt = params.select { |k,v| k =~ /^lt_/ }.select { |k, v| v }.keys.map { |e| e.gsub(/^lt_/,'') }.map { |e| "'#{e}'" }.join(', ')
    if params['lintian_tag'] and params['lintian_tag'] != ''
      tagname = "AND tag = '#{params["lintian_tag"]}'"
    else
      tagname = ""
    end
    @lintian = []
    # lintian
    q = <<-EOF
select source, version, package, package_version, architectures::text[], tag_type, tag, information, lintian_version, count
FROM lintian_results_agg
WHERE source in (select source from mysources)
AND tag_type IN (#{tt}) #{tagname}
ORDER BY 1,2,3,4,6,7
EOF
    @lintian = {}
    @lintian['all'] = dbget(q).map { |r| r.to_h }
    @lintian['all'].each do |e|
      # convert array to string (that's something that is only needed in the dev env, strangely)
      if defined?(Sequel::Postgres::PGArray) and e['architectures'].kind_of?(Sequel::Postgres::PGArray)
        e['architectures'] = e['architectures'].join(',')
      end
      e['architectures'] = e['architectures'].gsub('NULL', 'source').gsub(/^\{(.*)\}$/, '\1').split(',').uniq.sort
    end
    @lintian['current_lintian_version'] = dbget("select max(lintian_version) from lintian_logs").map { |r| r.to_h }.first.values.first
    q = <<-EOF
    select count(distinct(source, version))
    FROM sources_uniq l
    WHERE distribution = 'debian' and release in ('sid', 'experimental') and component = 'main'
    and NOT EXISTS (
      SELECT null
      FROM lintian_logs r
      WHERE l.source = r.source and l.version = r.version
    )
    EOF
    @lintian['unknown_sources'] = dbget(q).map { |r| r.to_h }.first.values.first
    @lintian
  end

  def get_qa
    @qa = {}
    return if @sources.empty?
    # reproducible
    q = "select source, version, release, architecture, status from reproducible where source in (select source from mysources)"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['reproducible'] ||= []
      @qa[r['source']]['reproducible'] << r.to_h
    end

    # lintian
    ## get maintainer email for each source. needed for lintian urls
    q = "select distinct source, maintainer_email from sources_uniq where release in ('sid', 'experimental') and source in (select source from mysources)"
    rows = dbget(q)
    maint = {}
    rows.each do |r|
      maint[r['source']] = r['maintainer_email']
    end
    ## get real lintian data
    q = <<-EOF
select source, tag_type, count(*) as count from
(
select package as source, tag_type, tag, information from lintian where package_type='source' and package in (select source from mysources)
UNION
select distinct ps.source as source, tag_type, tag, information from lintian, packages_summary ps where package_type='binary' and ps.package = lintian.package and ps.release in ('sid', 'experimental') and ps.source in (select source from mysources)) t
group by source, tag_type;
EOF
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['lintian'] ||= {}
      @qa[r['source']]['lintian']['maintainer'] = maint[r['source']]
      @qa[r['source']]['lintian'][r['tag_type']] = r['count'].to_i
    end
    # patches
    q = "select source, format, patches, forwarded_invalid, forwarded_no from sources_patches_status where source in (select source from mysources) and release='sid' order by source asc"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      if r['format'] == '3.0 (quilt)'
        @qa[r['source']]['patches'] = {'total' => r['patches'], 'needs-work' => r['forwarded_invalid'] + r['forwarded_no'] }
      end
    end
    # piuparts
    q = "select source, section, status from piuparts_status where source in (select source from mysources) and status = 'fail' and section NOT IN (#{PIUPARTS_IGNORED_SECTIONS})"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['piuparts'] ||= []
      @qa[r['source']]['piuparts'] << r['section']
    end
    # ci.debian.net
    q = "select suite, arch, source, status, message from ci where source in (select source from mysources)"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['ci'] ||= {}
      if r['source'] =~ /^lib/
        poold = r['source'][0..3]
      else
        poold = r['source'][0]
      end
      if r['status'] == 'pass' or r['status'] == 'neutral'
        prio = '<span class="prio_ok_nobold">'
        eprio = '</span>'
      elsif r['status'] == 'fail'
        prio = '<span class="prio_high_nobold">'
        eprio = '</span>'
      elsif r['status'] == 'tmpfail'
        prio = '<span class="prio_med_nobold">'
        eprio = '</span>'
      else
        raise 'Unhandled case'
      end
      text = "<a href=\"http://ci.debian.net/packages/#{poold}/#{ERB::Util::url_encode(r['source'])}/#{r['suite']}/#{r['arch']}/\" title=\"#{r['message']}\">#{prio}#{r['status']}#{eprio}</a>"
      @qa[r['source']]['ci'][r['suite']+'/'+r['arch']] = { :status => r['status'], :message => r['message'], :html => text }
    end
    # duck
    q = "select source from duck where source in (select source from mysources)"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['duck'] = true
    end

    # salsa
    q = "select source, browser, ci_status, ci_url, issues, merge_requests from vcswatch where source in (select source from mysources) and ci_status is not null or issues is not null or merge_requests is not null"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['salsa'] = r
    end

    q = "SELECT source, insts
         FROM popcon_src
         WHERE source IN (SELECT source FROM mysources)"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['popcon'] = r['insts']
    end

    q = "SELECT source, max(date) as last
         FROM upload_history
         WHERE source IN (SELECT source FROM mysources)
         group by source"
    rows = dbget(q)
    rows.each do |r|
      @qa[r['source']] ||= {}
      @qa[r['source']]['last_upload'] = r['last'].to_date.to_s
    end

    # security tracker
    q = <<-EOF
select sir.source, sir.release, sir.urgency, count(distinct sir.issue)
from security_issues_releases sir
where sir.status != 'resolved'
and source in (select source from mysources)
group by sir.source, sir.release, sir.urgency;
    EOF
    rows = dbget(q)
    rows.each do |r|
      source, release, urgency, count = r.values
      @qa[source] ||= {}
      @qa[source]['security'] ||= {}
      @qa[source]['security'][release] ||= {}
      @qa[source]['security'][release][urgency] = count
    end
    @qa.keys.select { |s| @qa[s].has_key?('security') }.each do |source|
      @qa[source]['security'].each_pair do |release, counts|
        counts2 = counts.dup
        if counts.has_key?('high') or counts.has_key?('high**')
          @qa[source]['security'][release]['prio'] = 'prio_high_nobold'
        elsif counts.has_key?('medium') or counts.has_key?('medium**')
          @qa[source]['security'][release]['prio'] = 'prio_medium_nobold'
        elsif counts.keys == ['unimportant']
          @qa[source]['security'][release]['prio'] = 'prio_ok_nobold'
        else
          @qa[source]['security'][release]['prio'] = ''
        end
        @qa[source]['security'][release]['total'] = counts2.to_a.inject(0) { |a,b| a + b[1] }
        @qa[source]['security'][release]['text'] = counts2.to_a.map { |a, b| "#{a}: #{b}" }.join(' ')
      end
    end
  end

  def get_ubuntu_bugs
    @ubuntu_bugs = {}
    return @ubuntu_bugs if @sources.empty?
    srcs = @sources.keys.map { |e| quote(e) }.join(',')
    q=<<-EOF
SELECT tbugs.package, bugs, patches
from (select package, count(distinct bugs.bug) as bugs
from ubuntu_bugs_tasks tasks,ubuntu_bugs bugs
where tasks.bug = bugs.bug
and distro in ('Ubuntu')
and status not in ('Invalid', 'Fix Released', 'Won''t Fix', 'Opinion')
and package in (select source from mysources)
group by package) tbugs
full join
(select package, count(distinct bugs.bug) as patches
from ubuntu_bugs_tasks tasks,ubuntu_bugs bugs
where tasks.bug = bugs.bug
and distro in ('', 'Ubuntu')
and status not in ('Invalid', 'Fix Released', 'Won''t Fix', 'Opinion')
and bugs.patches is true
and package in (select source from mysources)
group by package) tpatches on tbugs.package = tpatches.package order by package asc
    EOF
    rows = dbget(q)
    rows.each do |r|
      @ubuntu_bugs[r['package']] = { :bugs => r['bugs'], :patches => r['patches'] || 0 }
    end
  end

  def get_sources_bugs
    @all_bugs = []
    @bugs_tags = {}
    @bugs_count = {}
    return if @sources.empty?
    srcs = @sources.keys
    q = "select id, package, source, severity, title, last_modified, affects_stable, affects_testing, affects_unstable, affects_experimental, status from bugs where source in (select source from mysources)"
    allbugs = dbget(q)
    @all_bugs = allbugs.map { |r| r.to_h }
    ids = @all_bugs.map { |e| e['id'] }
    @bugs_tags = {}
    if not ids.empty?
      ids.each do |id|
        @bugs_tags[id] = []
      end
      tags = dbget("select id, tag from bugs_tags where id in (#{ids.join(',')})")
      tags.each do |r|
        @bugs_tags[r['id']] << r['tag']
      end
      # get release team status for bugs
      ['stable', 'testing', 'unstable'].each do |rel|
        dbget("select id from bugs_rt_affects_#{rel} where id in (#{ids.join(',')})").each do |r|
          @bugs_tags[r['id']] << "rt_affects_#{rel}"
        end
      end
    end

    @all_bugs.group_by { |b| b['source'] }.each_pair do |src, bugs|
      openbugs = bugs.select { |b| b['status'] != 'done' }
      rc_bugs = openbugs.select { |b| ['serious', 'grave', 'critical'].include?(b['severity']) }.count
      patches = openbugs.select { |b| (@bugs_tags[b['id']] || []).include?('patch') }.count
      pending = openbugs.select { |b| (@bugs_tags[b['id']] || []).include?('pending') }.count
      @bugs_count[src] = { :rc => rc_bugs, :all => openbugs.count, :patch => patches, :pending => pending }
    end
  end

  def get_ubuntu_dirty
      @ubuntu = Array.new
      @sources.keys.sort.each do |src|
        next if not versions.include?(src)
        next if (not versions[src].include?('debian') or not versions[src].include?('ubuntu'))

        ub = ubuntu_bugs[src]
        if ub.nil?
          bugs = 0
          patches = 0
        else
          bugs = ub[:bugs]
          patches = ub[:patches]
        end

        dv = versions[src]['debian']
        if dv['sid']
          sid = dv['sid'][:version]
        else
          sid = ''
        end

        du = versions[src]['ubuntu']
        ustb = []
        udev = ''
        if not du.nil?
          ustb << du[@ustb][:version] if du[@ustb]
          ustb << "sec:&nbsp;#{du["#{@ustb}-security"][:version]}" if du["#{@ustb}-security"]
          ustb << "upd:&nbsp;#{du["#{@ustb}-updates"][:version]}" if du["#{@ustb}-updates"]
          ustb << "prop:&nbsp;#{du["#{@ustb}-proposed"][:version]}" if du["#{@ustb}-proposed"]
          ustb << "bpo:&nbsp;#{du["#{@ustb}-backports"][:version]}" if du["#{@ustb}-backports"]

          udev = du[@udev][:version] if du[@udev]
          if udev == sid
            if bugs == 0 and patches == 0
              next
            end
          elsif sid != ''
            if UDDData.compare_versions(udev, sid) == -1
              if udev =~ /ubuntu/
                udev = "<a href=\"http://ubuntudiff.debian.net/?query=-FPackage+#{ERB::Util::url_encode(src)}\"><span class=\"prio_high\" title=\"Outdated version in Ubuntu, with an Ubuntu patch\">#{udev}</span></a>"
              else
                udev = "<span class=\"prio_med\" title=\"Outdated version in Ubuntu\">#{udev}</span>"
              end
            elsif UDDData.compare_versions(udev, sid) == 1
              udevnb = udev.gsub(/build\d+$/,'')
              if UDDData.compare_versions(udevnb, sid) == 1
                udev = "<a href=\"http://ubuntudiff.debian.net/?query=-FPackage+#{ERB::Util::url_encode(src)}\"><span class=\"prio_high\" title=\"Newer version in Ubuntu\">#{udev}</span></a>"
              end
            end
          end
          if udev == ''
            udev = []
          else
            udev = [ udev ]
          end
          udev << "sec:&nbsp;#{du["#{@udev}-security"][:version]}" if du["#{@udev}-security"]
          udev << "upd:&nbsp;#{du["#{@udev}-updates"][:version]}" if du["#{@udev}-updates"]
          udev << "prop:&nbsp;#{du["#{@udev}-proposed"][:version]}" if du["#{@udev}-proposed"]
          udev << "bpo:&nbsp;#{du["#{@udev}-backports"][:version]}" if du["#{@udev}-backports"]
        end

        versions = UDDData.colorize_versions(
          [
            ustb.join('<br>'),
            udev.join('<br>')
          ],
            ['#eeeeff', '#ffeeff']
        )
        @ubuntu.push({:src => src,
                      :bugs => bugs,
                      :patches => patches,
                      :pts => "https://tracker.debian.org/#{ERB::Util::url_encode(src)}",
                      :launchpad => "https://bugs.launchpad.net/ubuntu/+source/#{ERB::Util::url_encode(src)}",
                      :versions => versions
        })
        end
  end
  
  def get_migration
    @migration = {}
    return if @sources.empty?
    q =<<-EOF
select source, unstable_version, in_testing, current_date - in_testing as in_testing_age, sync, current_date - sync as sync_age,
current_date - first_seen as debian_age
from migrations where current_date - in_unstable < 2 and (sync is null or current_date - sync > 1)
and source in (select source from mysources)
and source not in (select source from upload_history where date > (current_date - interval '10 days') and distribution='unstable')
    EOF
    rows = dbget(q)
    rows.each do |r|
      @migration[r['source']] = {}
      @migration[r['source']]['unstable_version'] = r['unstable_version']
      @migration[r['source']]['in_testing_age'] = r['in_testing_age']
      @migration[r['source']]['in_testing'] = r['in_testing'] ? r['in_testing'].to_date : nil
      @migration[r['source']]['debian_age'] = r['debian_age']
      @migration[r['source']]['sync'] = r['sync'] ? r['sync'].to_date : nil
      @migration[r['source']]['sync_age'] = r['sync_age']
    end
  end

  def get_buildd
    @buildd = {}
    return if @sources.empty?
    q = <<-EOF
    select source, architecture, state, state_change from wannabuild
    where distribution='sid' and state not in ('Installed', 'Needs-Build', 'Dep-Wait', 'Not-For-Us', 'Auto-Not-For-Us')
    and (state not in ('Built', 'Uploaded', 'Building') or now() - state_change > interval '3 days')
    and notes <> 'uncompiled'
    and source in (select source from mysources)
    and not vancouvered
    EOF
    rows = dbget(q)
    rows.each do |r|
      @buildd[r['source']] ||= []
      @buildd[r['source']] << r.to_h
    end
  end

  def get_autoremovals
    @autoremovals = {}
    return if @sources.empty?
    q = "select source, version, bugs, removal_time from testing_autoremovals where source in (select source from mysources)"
    rows = dbget(q)
    rows.each do |r|
      @autoremovals[r['source']] = r.to_h
    end
  end

  def get_sources_dirty
    @sources.each do |s|
        h = Hash.new
        src = s[0]
        #reason = src_reason(src)
        h[src] = Hash.new
        h[src][:reason] = s[1][2]

        next if not versions[src]
        dv = versions[src]['debian']
        next if not dv
        t_oldstable = []
        t_stable = []
        t_testing = []
        t_unstable = []
        t_experimental = []
        t_vcs = []

        t_oldstable << dv[@dostb][:version] if dv[@dostb]
        t_oldstable << "sec: #{dv[@dostb+'-security'][:version]}" if dv[@dostb+'-security']
        t_oldstable << "upd: #{dv[@dostb+'-updates'][:version]}" if dv[@dostb+'-updates']
        t_oldstable << "pu: #{dv[@dostb+'-proposed-updates'][:version]}" if dv[@dostb+'-proposed-updates']
        t_oldstable << "bpo: #{dv[@dostb+'-backports'][:version]}" if dv[@dostb+'-backports']
        t_oldstable << "bpo-sl: #{dv[@dostb+'-backports-sloppy'][:version]}" if dv[@dostb+'-backports-sloppy']

        t_stable << dv[@dstb][:version] if dv[@dstb]
        t_stable << "sec: #{dv[@dstb+'-security'][:version]}" if dv[@dstb+'-security']
        t_stable << "upd: #{dv[@dstb+'-updates'][:version]}" if dv[@dstb+'-updates']
        t_stable << "pu: #{dv[@dstb+'-proposed-updates'][:version]}" if dv[@dstb+'-proposed-updates']
        t_stable << "bpo: #{dv[@dstb+'-backports'][:version]}" if dv[@dstb+'-backports']
        t_stable << "bpo-sl: #{dv[@dstb+'-backports-sloppy'][:version]}" if dv[@dstb+'-backports-sloppy']

        t_testing << dv[@dtst][:version] if dv[@dtst]
        t_testing << "sec: #{dv[@dtst+'-security'][:version]}" if dv[@dtst+'-security']
        t_testing << "upd: #{dv[@dtst+'-updates'][:version]}" if dv[@dtst+'-updates']
        t_testing << "pu: #{dv[@dtst+'-proposed-updates'][:version]}" if dv[@dtst+'-proposed-updates']

        if dv['sid']
          t_unstable << dv['sid'][:version]
          sid = dv['sid'][:version]
        else
          sid = ''
        end

        if dv['experimental']
          t_experimental << dv['experimental'][:version]
          exp = dv['experimental'][:version]
        else
          exp = ''
        end

        vcs = versions[src]['vcs']
        if vcs
          t = UDDData.compare_versions(sid, vcs[:version])
          te = UDDData.compare_versions(exp, vcs[:version])
          if (t == -1 or te == -1) and t != 0 and te != 0 and ['unstable', 'experimental'].include?(vcs[:distribution])
            t_vcs << "<a href=\"https://qa.debian.org/cgi-bin/vcswatch?package=#{ERB::Util::url_encode(src)}\"><span class=\"prio_high\" title=\"Ready for upload to #{vcs[:distribution]}\">#{vcs[:version]}</span></a>"
          elsif (t == -1 or te == -1) and t != 0 and te != 0
            t_vcs << "<a href=\"https://qa.debian.org/cgi-bin/vcswatch?package=#{ERB::Util::url_encode(src)}\"><span class=\"prio_med\" title=\"Work in progress\">#{vcs[:version]}</span></a>"
          elsif t == 1 and te == 1
            t_vcs << "<a href=\"https://qa.debian.org/cgi-bin/vcswatch?package=#{ERB::Util::url_encode(src)}\"><span class=\"prio_high\" title=\"Version in archive newer than version in VCS\">#{vcs[:version]}</span></a>"
          else
            t_vcs << "#{vcs[:version]}"
          end
        end

      h[src][:versions] = [
        t_oldstable.join('<br>'),
        t_stable.join('<br>'),
        t_testing.join('<br>'),
        t_unstable.join('<br>'),
        t_experimental.join('<br>'),
        t_vcs.join('<br>')
      ]
      colors = ['#ffeeff', # purple, oldstable
                '#eeeeff', # blue, stable
                '#eeffee', # green, testing
                '#ffeeee', # red, unstable
                '#ffffee', # yellow, experimental
                '#eeffff', # cyan, vcs
      ]
      # add <td> and color
      h[src][:versions] = UDDData.colorize_versions( h[src][:versions], colors)
      @sources[src] = h[src]
    end
  end

  def get_security_issues
    @security_issues = {}
    return if @sources.empty?
    q = "SELECT sir.source, sir.release, sir.urgency, sir.issue
           FROM security_issues_releases sir
           WHERE sir.status != 'resolved' AND urgency != 'unimportant' AND
                 sir.source IN (SELECT source FROM mysources) 
           GROUP BY sir.source, sir.release, sir.urgency, sir.issue;"
    rows = dbget(q)
    rows.each do |r|
      @security_issues[r['source']] = r.to_h
    end
  end

  def get_dmd_todos
    @dmd_todos = []
    rc_bugs = @all_bugs.select { |b| ['serious', 'grave', 'critical'].include?(b['severity']) }
    testing_rc_bugs = []
    stable_rc_bugs = []
    rc_bugs.each do |bug|
      id = bug['id']
      h = Digest::MD5.hexdigest("#{bug['source']}_#{id}")
      if bug['status'] == 'done' and @bugs_tags[id].include?('rt_affects_unstable')
        @dmd_todos << { :shortname => "rc_done_#{h}",
                        :type => 'RC bug',
                        :source => bug['source'],
                        :link => "https://bugs.debian.org/#{id}",
                        :description => "RC bug marked as done but still affects unstable",
                        :details =>" ##{id}: #{CGI.escape_html(bug['title'])}",
                        :updated => bug['last_modified'].to_time }
      elsif (not @bugs_tags[id].include?('rt_affects_unstable')) and @bugs_tags[id].include?('rt_affects_testing')
        testing_rc_bugs << { :shortname => "rc_testing_#{h}",
                             :type => 'RC bug',
                             :source => bug['source'],
                             :link => "https://bugs.debian.org/#{id}",
                             :description => "RC bug affecting testing only (ensure the package migrates)",
                             :details => "##{id}: #{CGI.escape_html(bug['title'])}",
                             :updated => bug['last_modified'].to_time }
      elsif @bugs_tags[id].include?('rt_affects_unstable') or @bugs_tags[id].include?('rt_affects_testing')
        @dmd_todos << { :shortname => "rc_std_#{h}",
                        :type => 'RC bug',
                        :source => bug['source'],
                        :link => "https://bugs.debian.org/#{id}",
                        :description => "RC bug needs fixing",
                        :details => "##{id}: #{CGI.escape_html(bug['title'])}",
                        :updated => bug['last_modified'].to_time }
      elsif @bugs_tags[id].include?('rt_affects_stable')
        stable_rc_bugs << { :shortname => "rc_stable_#{h}",
                            :type => 'RC bug (stable)',
                            :source => bug['source'],
                            :link => "https://bugs.debian.org/#{id}",
                            :description => "RC bug affecting stable",
                            :details => "##{id}: #{CGI.escape_html(bug['title'])}",
                            :updated => bug['last_modified'].to_time }
      end
    end
    @dmd_todos.concat(testing_rc_bugs)
    @dmd_todos.concat(stable_rc_bugs)

    @security_issues.each_pair do |src, v|
      # v: source, release, urgency, issue

      @dmd_todos << {
        :shortname => "security_issue_#{src}_#{v['release']}_#{v['issue']}",
        :type => 'security issue',
        :source => src,
        :link => "https://security-tracker.debian.org/tracker/#{v['issue']}",
        :description => "Open security issue",
        :details => "#{v['issue']} for #{v['release']} (#{v['urgency']} urgency)"
      }
    end

    @buildd.each_pair do |src, archs|
      archs.each do |arch|
        h = Digest::MD5.hexdigest("#{src}_#{arch.sort.to_s}")
        @dmd_todos << { :shortname => "missingbuild_#{h}",
                        :type => 'missing build',
                        :source => src,
                        :link => "https://buildd.debian.org/status/package.php?p=#{ERB::Util::url_encode(src)}",
                        :description => "Missing build on #{arch['architecture']}",
                        :details => " state <i>#{arch['state']}</i> since #{arch['state_change'].to_date.to_s}",
                        :updated => arch['state_change'].to_time }
      end
    end

    @migration.each_pair do |src, v|
      h = Digest::MD5.hexdigest("#{src}_#{v['unstable_version']}")
      sn = "migration_#{h}"
      if v['in_testing_age'].nil?
        if v['debian_age'] > MIN_AGE_IN_DEBIAN
        @dmd_todos << { :shortname => sn,
                        :type => 'testing migration',
                        :source => src,
                        :link => "https://qa.debian.org/excuses.php?package=#{ERB::Util::url_encode(src)}",
                        :description => "Migration",
                        :details => "Has been in Debian for #{v['debian_age']} days, but never migrated to testing" }
        end
      elsif v['in_testing_age'] > 1 # in case there's some incoherency in udd
        @dmd_todos << { :shortname => sn,
                        :type => 'testing migration',
                        :source => src,
                        :link => "https://qa.debian.org/excuses.php?package=#{ERB::Util::url_encode(src)}",
                        :description => "Migration",
                        :details => "Not in testing for #{v['in_testing_age']} days" }
      else
        if v['sync_age'].nil?
          #        puts "Interesting buggy case with #{pkg}. Ignore."
        elsif v['sync_age'] > MIN_SYNC_INTERVAL
        @dmd_todos << { :shortname => sn,
                        :type => 'testing migration',
                        :source => src,
                        :link => "https://qa.debian.org/excuses.php?package=#{ERB::Util::url_encode(src)}",
                        :description => "Migration",
                        :details => "Has been trying to migrate for #{v['sync_age']} days" }
        end
      end
    end

    @ready_for_upload.each_pair do |src, v|
      h = Digest::MD5.hexdigest("#{src}_#{v['changelog_version']}_#{v['changelog_distribution']}")
      @dmd_todos << { :shortname => "vcs_#{h}",
                      :type => 'vcs',
                      :source => src,
                      :link => v['browser'],
                      :description => "New version",
                      :details => "#{v['version']} ready for upload" }
    end

    @versions.each_pair do |src, v|
      next if not v.has_key?('upstream')
      if v['debian']['sid']
        cursid = " (currently in unstable: #{v['debian']['sid'][:version]})"
      else
        cursid = ""
      end
      if v.has_key?('fresh-releases')
        cursid += " (janitor.d.n fresh-releases could automatically generate <a href=\"https://janitor.debian.net/fresh-releases/pkg/#{src}\">#{v['fresh-releases'][:version]}</a>)"
      end

      if v['upstream'][:status] == :out_of_date
        h = Digest::MD5.hexdigest("#{src}_#{v['upstream'][:version]}")
        @dmd_todos << { :shortname => "newupstream_#{h}",
                        :type => 'new upstream',
                        :source => src,
                        :link => nil,
                        :description => "New upstream version available",
                        :details => "#{v['upstream'][:version]}#{cursid}" }
      elsif v['upstream'][:status] == :out_of_date_in_unstable
        h = Digest::MD5.hexdigest("#{src}_#{v['upstream'][:version]}")
        @dmd_todos << { :shortname => "newupstreamunstable_#{h}",
                        :type => 'new upstream',
                        :source => src,
                        :link => nil,
                        :description => "New upstream version available",
                        :details => "#{v['upstream'][:version]} (already in experimental, but not in unstable)#{cursid}" }
      elsif v['upstream'][:status] == :error
        h = Digest::MD5.hexdigest("#{src}_#{v['upstream'][:version]}_error")
        @dmd_todos << { :shortname => "scanerror_#{h}",
                        :type => 'uscan error',
                        :source => src,
                        :link => nil,
                        :description => "debian/watch: uscan returned an error",
                        :details => "#{v['upstream'][:warnings]}#{v['upstream'][:errors]}".gsub(/uscan warning: In (watchfile )?(\/dev\/shm\/[^,]+|debian\/watch),/, 'uscan warning:') }
      end
    end

    @versions.each_pair do |src, v|
      next if not v.has_key?('mentors')
      v = v['mentors']
      h = Digest::MD5.hexdigest("#{src}_#{v[:version]}#{v[:uploaded]}")
      @dmd_todos << { :shortname => "mentors_#{h}",
                      :type => 'mentors',
                      :source => src,
                      :link => "https://mentors.debian.net/package/#{ERB::Util::url_encode(src)}",
                      :description => "New version available on mentors",
                      :details => "#{v[:version]} (uploaded on #{v[:uploaded]})" }
    end


    @autoremovals.each_pair do |src, v|
      if v['bugs'] != nil
        bugs = v['bugs'].split(',').map { |b| "##{b}" }
        if bugs.count > 1
          bugs = " (bugs: #{bugs.join(', ')})"
        else
          bugs = " (bug: #{bugs[0]})"
        end
      else
        bugs = ""
      end

      @dmd_todos << {
        :shortname => "autoremoval_#{src}_#{v['version']}_#{v['removal_time']}",
        :type => 'testing auto-removal',
        :source => src,
        :link => nil,
        :description => "Testing auto-removal",
        :details => "on #{Time.at(v['removal_time']).to_date.to_s}#{bugs}"
      }
    end

    @dmd_todos.map do |t|
      t[:reason] = @sources[t[:source]][:reason]
      t
    end

    @dmd_todos
  end

  def complete_email(email)
    q = <<-EOF
SELECT DISTINCT ON (value) value, label
FROM (
    SELECT value, label, count(*)
    FROM (
      SELECT maintainer_email AS value, maintainer AS label
      FROM sources
      where release in ('sid', 'experimental', '#{@dtst}')
      union
      select email as value, uploader as label
      from uploaders
      where release in ('sid', 'experimental', '#{@dtst}')
    ) sq2
    GROUP BY value, label
    ORDER BY value, count DESC
) subquery
WHERE value ~* ? or label ~* ?
    EOF
    r = dbget(q, ".*#{email}.*", ".*#{email}.*")
    return r.map do |r|
      r = r.to_h
      r['value'].encode!('UTF-8', :undef => :replace, :invalid => :replace, :replace => "")
      r['label'].encode!('UTF-8', :undef => :replace, :invalid => :replace, :replace => "")
      r
    end
  end

  def UDDData.compare_versions(v1, v2)
    if not v1 or not v2
      return 0
    end
    if Debian::Dpkg::compare_versions(v1, 'lt', v2)
      return -1
    elsif Debian::Dpkg::compare_versions(v1, 'eq', v2)
      return 0
    else
      return 1
    end
  end

  def UDDData.colorize_versions(versions, colors)
    versions = versions.map do |e|
      # find color
      if e != ''
        c = versions.find_index { |v| v == e }
        color = colors[c]
      else
        color = nil
      end
      if color
        "<td style='background-color: #{color}'>#{e}</td>"
      else
        "<td>#{e}</td>"
      end
    end
    versions
  end

  def UDDData.group_values(*values)
    agg = []
    values.each do |v|
      if agg.empty? or v != agg.last[:value]
        agg << { :value => v, :count => 1 }
      else
        agg.last[:count] += 1
      end
    end
    return agg
  end

  private
  def dbget(q, *args)
    if @debug
      t = Time::now
      puts "<pre>at #{t}:\n#{q}</pre>"
      p args if not args.nil? and not args.empty?
    end
    rows = @dbh.fetch(q, *args).all.sym2str
    if @debug
      puts "<pre>"
      qtime = Time::now - t
      @totqtime += qtime
      puts "Query duration: #{qtime}s (total: #{@totqtime}s)"
#      pp rows
      puts "</pre>"
    end
    return rows
  end

  def quote(s)
    return @dbh.literal(s)
  end
end
