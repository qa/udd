/* Set up per-column search fields in DataTables */
function setupColumnSearch(table) {
    table.api().columns().every(function () {
        let column = this;
        let title = column.header().textContent;

        // Create input element for each column in <tfoot>
        let input = document.createElement('input');
        input.style.cssText = 'width: 100%; box-sizing: border-box';
        input.placeholder = "Search " + title;
        column.footer().replaceChildren(input);

        // Event listener for user input
        input.addEventListener('keyup', function () {
            if (column.search() !== input.value) {
                column.search(input.value).draw();
            }
        });
    });
}
