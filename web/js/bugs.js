$(document).ready(function () {
    $('#bugs').DataTable( {
      paging: false,
      /* No ordering applied by DataTables during initialisation -- https://stackoverflow.com/a/4964423 */
      order: []
    });
});
