// responsive table (not full width)
style = document.createElement("style");
style.innerHTML = `div.dt-container#btstags-users_wrapper { display: inline-table; }`;
document.head.appendChild(style);

style = document.createElement("style");
style.innerHTML = `div.dt-container#btstags-user_wrapper { display: inline-table; }`;
document.head.appendChild(style);

style = document.createElement("style");
style.innerHTML = `div.dt-container#btstags-bug_wrapper { display: inline-table; }`;
document.head.appendChild(style);

style = document.createElement("style");
style.innerHTML = `div.dt-container#btstags-usertag_wrapper { display: inline-table; }`;
document.head.appendChild(style);


$(document).ready(function () {
    $('#btstags-users').DataTable( {
      paging: false,
    });

    $('#btstags-user').DataTable( {
      paging: false
    });

    $('#btstags-bug').DataTable( {
      paging: false
    });


    // Check if <tfoot> exists, if not, create it
    var table = $("#btstags-usertag");
    if (table && table.find("tfoot").length === 0) {
        var tfoot = $("<tfoot>").append(table.find("thead tr").clone());
        // Insert <tfoot> right after <thead>
        tfoot.css('display', 'table-header-group');
        table.children("thead").after(tfoot);
    }

    $('#btstags-usertag').DataTable( {
      paging: false,
      initComplete: function () { setupColumnSearch(this); }
    });
});
