#!/usr/bin/ruby
# encoding: utf-8

require 'erb'

puts "Content-type: text/html\n\n"
pagename = '' ; title = 'Ultimate Debian Database'
puts ERB.new(File.read(File.expand_path('templates/partials/header.erb'))).result(binding)
puts <<-EOF
<div id="content">
    <h1>Ultimate Debian Database</h1>
    <p>Ultimate Debian Database (UDD) gathers various Debian data in the same SQL database. It allows users to easily access and combine all these data. Read more on <a href="https://wiki.debian.org/UltimateDebianDatabase">the Ultimate Debian Database wiki page</a>. Here are some UDD-based web services:</P>

<ul>
   <li><a class="http" href="/bugs/">Bugs Search</a>: multi-criteria search engine for bugs</li>
   <li><a class="http" href="/dmd/">Maintainer Dashboard</a>: information about teams and maintainers' packages</li>
   <li><a class="http" href="/lintian/">Lintian results</a> and <a class="http" href="/lintian-tags/">Lintian tags</a></li>
   <li><a class="http" href="/patches/">Patches</a> provides access to patches applied by Debian to upstream software</li>
   <li><a class="http" href="cgi-bin/bts-usertags.cgi">Bugs Usertags</a>: search for usertag on bugs</li>
   <li><a class="http" href="sponsorstats.cgi">Sponsoring Statistics</a> gives some statistics about who is sponsoring uploads to Debian</li>
</ul>
<p>The <a class="http" href="udd-status.cgi">UDD status</a> page lists the UDD importers, their current status and errors</li>
<br />
<br />
<br />
</div>
EOF

puts ERB.new(File.read(File.expand_path('templates/partials/footer.erb'))).result(binding)
