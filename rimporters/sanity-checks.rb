def update_sanity_checks
  db = PG.connect({ :dbname => 'udd', :port => 5452})

  # Missing tags in bts_tags (tags used in bugs, not found in bts_tags)
  tags = db.exec("select distinct tag from bugs_tags where tag not in (select tag from bts_tags)").to_a
  if tags.length > 0
    raise "Missing tags in bts_tags (please insert them manually): #{tags.join(' ')}"
  end
  
  # Missing Debian release
  releases = db.exec("select distinct release from sources where release not in (select release from releases)").to_a
  if releases.length > 0
    raise "Missing releases in releases table (please insert them manually): #{releases.join(' ')}"
  end

  # Lintian version on worker
  worker_version = db.exec("select max(lintian_version) from lintian_results").first.values.first
  lintian_version = db.exec("select version from sources where source='lintian' and release='sid'").first.values.first
  if worker_version != lintian_version
    raise "Different lintian version on worker (#{worker_version}) and in sid (#{lintian_version})"
  end
end
