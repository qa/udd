require 'udd-db'
require 'pp'
require 'cgi'
require 'json'
require 'yaml'
require 'time'
require 'erb'
require 'uri'

BUG_SEVERITIES = %w{wishlist minor normal important serious grave critical}
def severity2sort(severity)
  if BUG_SEVERITIES.include?(severity.downcase)
    return BUG_SEVERITIES.index(severity.downcase)
  else
    return -1
  end
end
