require 'sequel'

# Sequel doesn't understand the 'service' keyword...
# https://github.com/jeremyevans/sequel/issues/1558
# Please switch to the following line once sequel has it,
# (5.13.0+) and remove this comment block. thanks.
#UDD_GUEST = {:adapter=>'postgres', :driver_options=>{:service=>'udd'}}
UDD_GUEST = 'postgres://localhost:5452/udd?user=guest'
UDD_USER = {:adapter=>'postgres', :database=>'udd', :port=>5452, :user=>'udd'}
UDD_USER_PG = {:dbname=>'udd', :port=>5452, :user=>'udd'}

class Array
  def sym2str
    return self.map do |e|
      result = {}
      e.each_key do |key|
        result[key.to_s] = e[key]
      end
      result
    end
  end

  def hash_values
    return self.map do |e|
      e.values
    end
  end
end
