This directory contains manually-generated dumps of UDD's schema, mostly to track changes over time.

To re-create a UDD database, it is better to start from a daily dump.

Generated using:
```
pg_dump -s udd > udd-schema.sql
pg_dump -a -t releases -t bts_tags udd > udd-data.sql
```
