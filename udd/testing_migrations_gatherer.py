# Last-Modified: <Sun Aug 10 12:16:12 2008>

# This file is a part of the Ultimate Debian Database Project

from .gatherer import gatherer

ZERO_DATE = '0000-01-01'

def get_gatherer(config, connection, source):
  return testing_migrations_gatherer(config, connection, source)


class testing_migrations_gatherer(gatherer):
  """This class imports testing migrations data into the database.

  For the files, see http://qa.debian.org/~lucas/testing-status.raw"""
  def __init__(self, connection, config, source):
    gatherer.__init__(self, connection, config, source)
    self.assert_my_config('path')

  def run(self):
    src_cfg = self.my_config

    c = self.connection.cursor()

    c.execute("DELETE FROM migrations")

    c.execute("PREPARE mig_insert AS INSERT INTO migrations (source, in_testing, testing_version, in_unstable, unstable_version, sync, sync_version, first_seen) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)")
      
    f = open(src_cfg['path'])
    field_names = ('package', 'in_testing', 'testing_version', 'in_unstable', 'unstable_version', 'sync', 'sync_version', 'first_seen')
    for line in f:
      fields = dict(zip(field_names, line.split()))
      for field in ('in_testing', 'in_unstable', 'sync', 'first_seen'):
        if fields[field] == ZERO_DATE:
          fields[field] = None

      for field in ('package', 'testing_version', 'unstable_version', 'sync_version'):
        if fields[field] == '-':
          fields[field] = None
        
      c.execute("EXECUTE mig_insert(%(package)s, %(in_testing)s, %(testing_version)s, %(in_unstable)s, %(unstable_version)s, %(sync)s, %(sync_version)s, %(first_seen)s)", fields)

    c.execute("DEALLOCATE mig_insert")
    c.execute("ANALYZE migrations")

