CREATE TABLE public.unofficial_packages (
    package text NOT NULL,
    version public.debversion NOT NULL,
    architecture text NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    description text,
    description_md5 text,
    source text,
    source_version public.debversion,
    essential text,
    depends text,
    recommends text,
    suggests text,
    enhances text,
    pre_depends text,
    breaks text,
    installed_size bigint,
    homepage text,
    size bigint,
    build_essential text,
    origin text,
    sha1 text,
    replaces text,
    section text,
    md5sum text,
    bugs text,
    priority text,
    tag text,
    task text,
    python_version text,
    ruby_versions text,
    provides text,
    conflicts text,
    sha256 text,
    original_maintainer text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    multi_arch text,
    package_type text,
    filename text
);
CREATE TABLE public.unofficial_packages_distrelcomparch (
    distribution text,
    release text,
    component text,
    architecture text
);
CREATE TABLE public.unofficial_sources (
    source text NOT NULL,
    version public.debversion NOT NULL,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    format text,
    files text,
    uploaders text,
    bin text,
    architecture text,
    standards_version text,
    homepage text,
    build_depends text,
    build_depends_indep text,
    build_conflicts text,
    build_conflicts_indep text,
    priority text,
    section text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    vcs_type text,
    vcs_url text,
    vcs_browser text,
    python_version text,
    ruby_versions text,
    checksums_sha1 text,
    checksums_sha256 text,
    original_maintainer text,
    dm_upload_allowed boolean,
    testsuite text,
    autobuild text,
    extra_source_only boolean,
    build_depends_arch text,
    build_conflicts_arch text
);
CREATE TABLE public.unofficial_descriptions (
    package text NOT NULL,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL,
    language text NOT NULL,
    description text NOT NULL,
    long_description text NOT NULL,
    description_md5 text NOT NULL
);
CREATE TABLE public.unofficial_packages_summary (
    package text NOT NULL,
    version public.debversion NOT NULL,
    source text,
    source_version public.debversion,
    maintainer text,
    maintainer_name text,
    maintainer_email text,
    distribution text NOT NULL,
    release text NOT NULL,
    component text NOT NULL
);
CREATE TABLE public.unofficial_uploaders (
    source text,
    version public.debversion,
    distribution text,
    release text,
    component text,
    uploader text,
    name text,
    email text
);
ALTER TABLE ONLY public.unofficial_descriptions
    ADD CONSTRAINT unofficial_descriptions_pkey PRIMARY KEY (package, distribution, release, component, language, description, description_md5);
ALTER TABLE ONLY public.unofficial_packages
    ADD CONSTRAINT unofficial_packages_pkey PRIMARY KEY (package, version, architecture, distribution, release, component);
ALTER TABLE ONLY public.unofficial_packages_summary
    ADD CONSTRAINT unofficial_packages_summary_pkey PRIMARY KEY (package, version, distribution, release, component);
ALTER TABLE ONLY public.unofficial_sources
    ADD CONSTRAINT unofficial_sources_pkey PRIMARY KEY (source, version, distribution, release, component);
CREATE INDEX unofficial_packages_distrelcomp_idx ON public.unofficial_packages USING btree (distribution, release, component);
CREATE INDEX unofficial_packages_source_idx ON public.unofficial_packages USING btree (source);
CREATE INDEX unofficial_packages_summary_distrelcompsrcver_idx ON public.unofficial_packages_summary USING btree (distribution, release, component, source, source_version);
CREATE INDEX unofficial_sources_distrelcomp_idx ON public.unofficial_sources USING btree (distribution, release, component);
CREATE INDEX unofficial_uploaders_distrelcompsrcver_idx ON public.unofficial_uploaders USING btree (distribution, release, component, source, version);
ALTER TABLE ONLY public.unofficial_packages
    ADD CONSTRAINT unofficial_packages_package_fkey FOREIGN KEY (package, version, distribution, release, component) REFERENCES public.unofficial_packages_summary(package, version, distribution, release, component) DEFERRABLE;

GRANT SELECT ON TABLE public.unofficial_packages TO PUBLIC;
GRANT SELECT ON TABLE public.unofficial_packages_distrelcomparch TO PUBLIC;
GRANT SELECT ON TABLE public.unofficial_sources TO PUBLIC;
GRANT SELECT ON TABLE public.unofficial_descriptions TO PUBLIC;
GRANT SELECT ON TABLE public.unofficial_packages_summary TO PUBLIC;
GRANT SELECT ON TABLE public.unofficial_uploaders TO PUBLIC;
